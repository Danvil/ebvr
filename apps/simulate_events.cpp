#include <chrono>
#include <cmath>
#include <type_traits>
#include <vector>

#include <Eigen/Core>
#include <GLFW/glfw3.h>
#include <gflags/gflags.h>

#include <dog/Camera.hpp>
#include <dog/dog.hpp>
#include <dog/GlfwTarget.hpp>
#include <dog/OffscreenRendering.hpp>
#include <dog/OpenGL2Renderer.hpp>
#include <Edvs/EventIO.hpp>

#include <common/camera.hpp>
#include <common/event.hpp>
#include <common/interpolate.hpp>
#include <common/pose3.hpp>
#include <common/simulate.hpp>
#include <common/timeseries.hpp>

namespace ebvr {
namespace {

/** @brief Renders a simulated test world with a view quads */
void renderTestWorld(const Pose3f& world_T_camera, const Eigen::Matrix4f& projection) {
	static dog::ClassicRenderer glr;
	dog::TriangleMesh mesh;
	mesh.addQuad(
		dog::VertexPC{{-1,0,-1},{1,1,1}},
		dog::VertexPC{{+1,0,-1},{1,1,1}},
		dog::VertexPC{{+1,0,+1},{1,1,1}},
		dog::VertexPC{{-1,0,+1},{1,1,1}});
	mesh.addQuad(
		dog::VertexPC{{-1,0,-1},{1,1,1}},
		dog::VertexPC{{+1,0,+1},{1,1,1}},
		dog::VertexPC{{+1,0,-1},{1,1,1}},
		dog::VertexPC{{-1,0,+1},{1,1,1}});
	for(int y=-3; y<=+3; y++) {
		for(int x=-3; x<=+3; x++) {
			bool is_solid = ((x + y) % 2 == 0);
			const float xf = static_cast<float>(x);
			const float yf = static_cast<float>(y);
			if(is_solid) {
				mesh.addQuad(
					dog::VertexPC{{xf-0.5f,yf-0.5f,0},{0.4f,0.4f,0.4f}},
					dog::VertexPC{{xf+0.5f,yf-0.5f,0},{0.4f,0.4f,0.4f}},
					dog::VertexPC{{xf+0.5f,yf+0.5f,0},{0.4f,0.4f,0.4f}},
					dog::VertexPC{{xf-0.5f,yf+0.5f,0},{0.4f,0.4f,0.4f}});
			}
		}
	}
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(&projection(0,0));
	glMatrixMode(GL_MODELVIEW);
	Eigen::Matrix4f mv = world_T_camera.matrix();
	glLoadMatrixf(&mv(0,0));
	glr.render(dog::cook(mesh));
}

// slimage::Image3ub visualizeEventfield(const EventField<double>& eventfield, double current_timestamp) {
// 	slimage::Image3ub img(eventfield.cols(), eventfield.rows());
// 	for(size_t i=0; i<img.size(); i++) {
// 		const double q = eventfield.data()[i];
// 		const double intensity = std::exp(-(current_timestamp - std::abs(q))/0.1f);
// 		float v = 0.5f + static_cast<float>(intensity)*(q > 0 ? 0.5f : -0.5f);
// 		unsigned char c = static_cast<unsigned char>(255.0f*v);
// 		img[i] = {c,c,c};
// 	}
// 	return img;
// }

// void markEvents(EventField<double>& eventfield, const std::vector<Timed<Event>>& events) {
// 	for(const auto& event : events) {
// 		eventfield(event.value.y, event.value.x) = seconds(event.timestamp) * (event.value.flag > 0 ? +1.0 : -1.0);
// 	}
// }

DEFINE_string(output, "/tmp/events.csv", "Filename where to save computed events");

template <typename CameraModel>
void simulate(const CameraModel& camera_model) {
	// Create simulated scenario.
	Timeseries<Pose3f> camera_poses;
	const Eigen::Vector3f target{0,0,0};
	const Eigen::Vector3f up{0,0,1};
	for(int i=0; i<20*1; i++) {
		const int64_t timestamp = i * 50 * MILLISECOND;
		const float angle = seconds(timestamp) * 45.0f * M_PI / 180.0f;
		const Eigen::Vector3f eye{5*std::cos(angle), 5*std::sin(angle), 1.7f};
		camera_poses.emplace_back(Timed<Pose3f>{lookAt(eye, target, up), timestamp});
	};
	// Run simulation to create events.
	auto start = std::chrono::high_resolution_clock::now();
	Timeseries<Event> events = simulateEvents(renderTestWorld, camera_poses, camera_model);
	auto end = std::chrono::high_resolution_clock::now();
	std::cout << "Simulated " << events.size() << " events "
	    << "in " << std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count() << " seconds" << std::endl;
	// Write events to file.
	WriteToFile(FLAGS_output, events);
}

/** @brief Main entry point for 'ebvr_sim' event simulation */
int main(int argc, char** argv) {
	google::ParseCommandLineFlags(&argc, &argv, true);
	dog::initialize(dog::CreateOpenGlTarget());
	// PinholeCameraModel camera_model{64.0f, {64.0f, 64.0f}};
	StereoPinholeCameraModel camera_model{
		{64.0f, {64.0f, 64.0f}},
		{64.0f, {64.0f, 64.0f}},
		Pose3f::Translation({-0.05f,0.0f,0.0f}),
		Pose3f::Translation({+0.05f,0.0f,0.0f})
	};
	simulate(camera_model);	
	dog::finalize();
	return 0;
}

} // namespace
} // namespace ebvr

int main(int argc, char** argv) {
	return ebvr::main(argc, argv);
}
