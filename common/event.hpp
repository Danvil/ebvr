#pragma once

#include <cstdint>
#include <string>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Core>

#include <dog/assert.hpp>

#include "timeseries.hpp"

namespace ebvr {

/** @brief An pixel event
 * Size: 8 + 2*2 + 1 + 1 = 14 byte
 */
struct Event {
	uint16_t x, y;
	uint8_t flag;
	uint8_t id;
};

/** @brief A stereo pixel event */
struct StereoEvent {
	float disparity;
	Eigen::Vector3f point;
};

/** @brief Writes events to a stream */
template<typename OutputStream>
OutputStream& WriteToStream(OutputStream& os, const std::vector<Timed<Event>>& events) {
	for(const Timed<Event>& event : events) {
		os
			<< event.timestamp << ","
			<< event.value.x << ","
			<< event.value.y << ","
			<< static_cast<unsigned>(event.value.flag) << ","
			<< static_cast<unsigned>(event.value.id) << "\n";
	}
	return os;
}

/** @brief Writes events to a file */
void WriteToFile(const std::string& filename, const std::vector<Timed<Event>>& events);

/** @brief Read events from a stream */
template<typename InputStream>
InputStream& ReadFromStream(InputStream& is, std::vector<Timed<Event>>& events) {
	std::string line;
	std::vector<std::string> tokens;
	while(std::getline(is, line)) {
		if(line.empty()) {
			continue;
		}
		boost::split(tokens, line, boost::is_any_of(","));
		dog::enforce(tokens.size() == 5, "Error parsing line");
		Timed<Event> event;
		event.timestamp = boost::lexical_cast<uint64_t>(tokens[0]);
		event.value.x = boost::lexical_cast<uint16_t>(tokens[1]);
		event.value.y = boost::lexical_cast<uint16_t>(tokens[2]);
		event.value.flag = boost::lexical_cast<uint8_t>(tokens[3]);
		event.value.id = boost::lexical_cast<uint8_t>(tokens[4]);
	}
	return is;
}

/** @brief Read events from a stream */
void ReadFromFile(const std::string& filename, std::vector<Timed<Event>>& events);

} // ebvr
