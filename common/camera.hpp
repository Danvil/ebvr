#pragma once

#include <Eigen/Core>

#include <dog/Camera.hpp>

#include "pose3.hpp"

namespace ebvr {

/** @brief A pinhole camera model */
struct PinholeCameraModel
{
	float focal_length_px;
	Eigen::Vector2f center;

	/** @brief Projects 3D point onto screen */
	Eigen::Vector2f project(const Eigen::Vector3f& point) const {
		const float scl = focal_length_px / point.z();
		return center + scl * point.block<2,1>(0,0);
	}

	/** @brief Computes 3D point from 2D screen coordinates and depth */
	Eigen::Vector3f backproject(int x, int y, float depth) const {
		const Eigen::Vector2f uv{static_cast<float>(x), static_cast<float>(y)};
		const float scl = depth / focal_length_px;
		const Eigen::Vector2f xy = scl * (uv - center);
		return { xy.x(), xy.y(), depth };
	}

	Eigen::Matrix4f openGlProjectionMatrix() const {
		// FIXME Use camera parameter to compute matrix
		return dog::PerspectiveProjection(M_PI*60.0f/180.0f, 1.0f, 0.1f, 100.0f);
	}

};

/** @brief A stereo pinhole camera model */
struct StereoPinholeCameraModel
{
	PinholeCameraModel left, right;
	Pose3f pose_left, pose_right;

};

} // namespace ebvr
