#pragma once

#include <common/camera.hpp>
#include <common/event.hpp>
#include <common/pose3.hpp>
#include <common/timeseries.hpp>

namespace ebvr {

/** @brief A function rendering something */
using RenderFunction = std::function<void(const Pose3f&, const Eigen::Matrix4f&)>;

/** @brief Simulates events for a given camera trajectory */
Timeseries<Event> simulateEvents(RenderFunction render_function, const Timeseries<Pose3f>& camera_poses, const PinholeCameraModel& camera_model);

/** @brief Simulates events for a given camera trajectory */
Timeseries<Event> simulateEvents(RenderFunction render_function, const Timeseries<Pose3f>& camera_poses, const StereoPinholeCameraModel& stereo_camera_model);

} // namespace ebvr
