#include "simulate.hpp"

#include <vector>
#include <algorithm>

#include <Eigen/Core>

#include <dog/dog.hpp>
#include <dog/OffscreenRendering.hpp>

#include <common/camera.hpp>
#include <common/event.hpp>
#include <common/interpolate.hpp>
#include <common/pose3.hpp>
#include <common/timeseries.hpp>

namespace ebvr {

/** @brief Simulates events for an event-based sensor */
class EventSimulation {
public:
	EventSimulation() {
		state_ = EventField<float>::Constant(SIZE, SIZE, 0.0f);
	}

	/** @brief Simulates events for a given camera trajectory */
	Timeseries<Event> simulateEvents(RenderFunction render_function, const Timeseries<Pose3f>& camera_poses, const Pose3f& camera_pose_offset, const PinholeCameraModel& camera_model, uint8_t id) {
		constexpr unsigned BATCH_SIZE = 100;
		constexpr int64_t SAMPLE_DT = MILLISECOND;
		const Eigen::Matrix4f projection = camera_model.openGlProjectionMatrix();
		dog::enforce(!camera_poses.empty());
		int64_t current_timestamp = camera_poses.front().timestamp;
		const int64_t target_timestamp = camera_poses.back().timestamp;
		Timeseries<Event> events;
		// EventField<double> event_field = EventField<double>::Constant(SIZE, SIZE, 0.0);
		slimage::Image3ub previous_rendering(RENDER_SIZE, RENDER_SIZE);
		std::fill(previous_rendering.begin(), previous_rendering.end(), slimage::Pixel3ub{128,128,128});
		auto rnd_dt = [this]() {
			static std::uniform_int_distribution<int64_t> rnd(0, SAMPLE_DT);
			return rnd(rnd_engine_);
		};
		auto render_f = [&camera_poses,&camera_pose_offset,&projection,&render_function](int64_t t) {
			render_function(camera_pose_offset * interpolate(camera_poses, t), projection);
		};
		// FIXME target_timestamp will not be computed
		for(; current_timestamp < target_timestamp; ) {
			// std::cout << current_timestamp << ":" << std::endl;
			unsigned batch_size = std::min<unsigned>(BATCH_SIZE, (target_timestamp - current_timestamp) / SAMPLE_DT);
			dog::RenderOffscreen(
				[&render_f,&current_timestamp](int i) {
					render_f(current_timestamp);
				},
				BATCH_SIZE,
				RENDER_SIZE, RENDER_SIZE,
				[this,&previous_rendering,&events,&current_timestamp,&rnd_dt,id](const dog::Frame& frame) {
					dog::feed({"offscreen color", current_timestamp}, frame.color);
					dog::feed({"offscreen depth", current_timestamp}, frame.depth);
					// Compute events by comparing to previous rendering
					std::vector<Event> now_events = diffEvents(previous_rendering, frame.color, id);
					previous_rendering = frame.color;
					std::vector<Timed<Event>> timed_now_events;
					for(const auto& event : now_events) {
						timed_now_events.emplace_back(Timed<Event>{event, current_timestamp + rnd_dt()});
					}
					current_timestamp += SAMPLE_DT;
					std::sort(timed_now_events.begin(), timed_now_events.end(), sortByTime<Event>);
					events.insert(events.end(), timed_now_events.begin(), timed_now_events.end());
				});
			// // visualize events
			// dog::feed({"events", current_timestamp}, [&]() {
			// 	markEvents(event_field, timed_now_events);
			// 	return visualizeEventfield(event_field, seconds(current_timestamp));
			// });
		}
		return events;
	}

private:
	std::default_random_engine rnd_engine_;

	/** @brief A 2D table of events */
	template<typename T>
	using EventField = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

	static constexpr int SUBPXSCL = 4;
	static constexpr float THRESHOLD = 0.1f;
	static constexpr int SIZE = 128;
	static constexpr int RENDER_SIZE = SIZE * SUBPXSCL;

	/** @brief Converts rgb to grey scale (equal weights) */
	float grey(const unsigned char* px) {
		return static_cast<float>(
			static_cast<int>(px[0]) +
			static_cast<int>(px[1]) +
			static_cast<int>(px[2])) / 3.0f;
	}

	/** @brief Computes events be comparing two rendered images */
	std::vector<Event> diffEvents(const slimage::Image3ub& previous, const slimage::Image3ub& now, uint8_t id) {
		const int w = previous.width();
		const int h = previous.height();
		const int ww = w / SUBPXSCL;
		const int hh = h / SUBPXSCL;
		const ssize_t img_diff = now.pixel_pointer(0,0) - previous.pixel_pointer(0,0);
		const size_t line_skip = 3*(w - SUBPXSCL);
		std::vector<Event> events;
		events.reserve(128);
		for(int yy=0; yy<hh; yy++) {
			for(int xx=0; xx<ww; xx++) {
				float diff = 0.0f;
				auto it = previous.pixel_pointer(xx*SUBPXSCL, yy*SUBPXSCL);
				for(int y=0; y<SUBPXSCL; y++) {
					for(int x=0; x<SUBPXSCL; x++) {
						diff += grey(it + img_diff) - grey(it);
						it += 3;
					}
					it += line_skip;
				}
				diff /= static_cast<float>(SUBPXSCL * SUBPXSCL);
				float& s = state_(yy,xx);
				s += diff;
				if(std::abs(s) > THRESHOLD) {
					const uint8_t flag = (s > 0.0f) ? 1 : 0;
					events.emplace_back(Event{static_cast<uint16_t>(xx), static_cast<uint16_t>(yy), flag, id});
					s = 0.0f;
				}
			}
		}
		return events;
	}

private:
	EventField<float> state_;
};

Timeseries<Event> simulateEvents(RenderFunction render_function, const Timeseries<Pose3f>& camera_poses, const Pose3f& camera_pose_offset, const PinholeCameraModel& camera_model, uint8_t id) {
	EventSimulation sim;
	return sim.simulateEvents(render_function, camera_poses, camera_pose_offset, camera_model, id);
}

Timeseries<Event> simulateEvents(RenderFunction render_function, const Timeseries<Pose3f>& camera_poses, const PinholeCameraModel& camera_model) {
	return simulateEvents(render_function, camera_poses, Pose3f::Identity(), camera_model, 0);
}

Timeseries<Event> simulateEvents(RenderFunction render_function, const Timeseries<Pose3f>& camera_poses, const StereoPinholeCameraModel& stereo_camera_model) {
	Timeseries<Event> events = simulateEvents(render_function, camera_poses, stereo_camera_model.pose_left, stereo_camera_model.left, 0);
	Timeseries<Event> events_right = simulateEvents(render_function, camera_poses, stereo_camera_model.pose_right, stereo_camera_model.right, 1);
	events.insert(events.end(), events_right.begin(), events_right.end());
	std::sort(events.begin(), events.end(), sortByTime<Event>);
	return events;
}

} // namespace ebvr
