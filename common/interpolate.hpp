#pragma once

#include <type_traits>

#include <Eigen/Core>

#include "pose3.hpp"

namespace ebvr {

/** @brief General linear interpolation */
template<typename T>
T interpolate(const T& a, const T& b, double p) {
	return (1.0 - p) * a + p * b;
}

/** @brief Interpolates matrices linearly */
template<typename T, int ROWS, int COLS>
typename std::enable_if<std::is_floating_point<T>::value, Eigen::Matrix<T, ROWS, COLS>>::type interpolate(const Eigen::Matrix<T, ROWS, COLS>& a, const Eigen::Matrix<T, ROWS, COLS>& b, double p) {
	return static_cast<T>(1.0 - p) * a + static_cast<T>(p) * b;
}

/** @brief Interpolates matrices linearly */
template<typename T, int ROWS, int COLS>
typename std::enable_if<!std::is_floating_point<T>::value, Eigen::Matrix<T, ROWS, COLS>>::type interpolate(const Eigen::Matrix<T, ROWS, COLS>& a, const Eigen::Matrix<T, ROWS, COLS>& b, double p) {
	Eigen::Matrix<T, ROWS, COLS> r(a.rows(), a.cols());
	const auto& aa = a.array();
	const auto& ab = b.array();
	const auto& ar = r.array();
	for(size_t i=0; i<aa.size(); i++) {
		ar[i] = interpolate(aa[i], ab[i], p);
	}
	return r;
}

/** @brief Interpolates quaternions linearly */
template<typename T>
Eigen::Quaternion<T> interpolate(const Eigen::Quaternion<T>& a, const Eigen::Quaternion<T>& b, double p) {
	return a.slerp(p, b);
}

/** @brief Interpolates 3D rotation linearly */
template<typename T>
So3<T> interpolate(const So3<T>& a, const So3<T>& b, double p) {
	return So3<T>(interpolate(a.quaternion(), b.quaternion(), p));
}

/** @brief Interpolates posed linearly */
template<typename T>
Pose3<T> interpolate(const Pose3<T>& a, const Pose3<T>& b, double p) {
	return {
		interpolate(a.translation, b.translation, p),
		interpolate(a.rotation, b.rotation, p)
	};
}

} // ebvr
