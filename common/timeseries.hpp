#pragma once

#include <type_traits>
#include <algorithm>
#include <vector>

#include <Eigen/Core>

#include <dog/assert.hpp>

namespace ebvr {

/** @brief A timed value */
template<typename Value>
struct Timed {
	// value at given time
	Value value;
	// timestamp in mus (1e-6 seconds)
	int64_t timestamp;	
};

/** @brief Sort timed values by timestamp */
template<typename Value>
bool sortByTime(const Timed<Value>& a, const Timed<Value>& b) {
	return a.timestamp < b.timestamp;
}

/** @brief Interpolates linearly between two timed values */
template<typename T>
T interpolate(const Timed<T>& a, const Timed<T>& b, uint64_t t) {
	dog::enforce(a.timestamp <= t && t <= b.timestamp, "Timestamp out of range");
	const double p = static_cast<double>(t - a.timestamp) / static_cast<double>(b.timestamp - a.timestamp);
	return interpolate(a.value, b.value, p);
}

/** @brief A series of timestamped values */
template<typename T>
using Timeseries = std::vector<Timed<T>>;

constexpr int64_t SECOND = 1000000;
constexpr int64_t MILLISECOND = 1000;
constexpr uint MICORSECOND = 1;

/** @brief Converts a timestamp into seconds */
inline
double seconds(int64_t t) {
	return static_cast<double>(t) / static_cast<double>(SECOND);
}

/** @brief Interpolates linearly from the time series */
template<typename T>
T interpolate(const Timeseries<T>& series, uint64_t timestamp) {
	dog::enforce(!series.empty(), "Series must not be empty");
	if(timestamp <= series.front().timestamp) {
		return series.front().value;
	}
	if(series.back().timestamp <= timestamp) {
		return series.back().value;
	}
	auto it = std::lower_bound(series.begin(), series.end(), timestamp, 
		[](const Timed<T>& v, uint64_t timestamp) {
			return v.timestamp < timestamp;
		});
	dog::enforce(it != series.begin() && it != series.end(), "Logic error");
	return interpolate(*std::prev(it), *it, timestamp);
}

} // ebvr
