#pragma once

#include <iostream>

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace ebvr {

template<typename T>
using Vector3 = Eigen::Matrix<T, 3, 1>;

/** @brief A 3D rotation */
template<typename T>
struct So3 {
	using Quaternion = Eigen::Quaternion<T>;

	So3() {}

	So3(const Quaternion& q)
	: quaternion_(q.normalized()) {}

	const Quaternion& quaternion() const { return quaternion_; }

	/** @brief Inverse */
	So3 inverse() const {
		return So3(quaternion_.inverse());
	}

	/** @brief Computes 3x3 rotation matrix */
	Eigen::Matrix<T,3,3> matrix() const {
		return quaternion_.matrix();
	}

	/** @brief Gives identity */
	static So3 Identity() {
		return Quaternion{1,0,0,0};
	}

	/** @brief Creates a rotation using axis/angle representation */
	static So3 AxisAngle(const Vector3<T>& axis, T angle) {
		return So3(Eigen::AngleAxis<T>(angle, axis));
	}

private:
	Quaternion quaternion_;
};

template<typename T>
std::ostream& operator<<(std::ostream& os, const So3<T>& so3) {
	const auto& q = so3.quaternion();
	os << "So3(" << q.w() << "," << q.x() << "," << q.y() << "," << q.z() << ")";
	return os;
}

/** @brief Rotates a vector */
template<typename T>
Vector3<T> operator*(const So3<T>& rotation, const Vector3<T>& p) {
	return rotation.quaternion()._transformVector(p);
}

/** @brief Multiplies two rotations */
template<typename T>
So3<T> operator*(const So3<T>& a, const So3<T>& b) {
	return So3<T>(a.quaternion() * b.quaternion());
}

/** @brief A 3D transformation */
template<typename T>
struct Pose3 {
	Vector3<T> translation;
	So3<T> rotation;

	/** @brief Computes inverse rotation */
	Pose3 inverse() const {
		So3<T> rotation_inverse = rotation.inverse();
		return {
			- (rotation_inverse*translation),
			rotation_inverse
		};
	}

	/** @brief Computes 4x4 affine matrix */
	Eigen::Matrix<T,4,4> matrix() const {
		Eigen::Matrix<T,4,4> result = Eigen::Matrix<T,4,4>::Identity();
		result.template block<3,1>(0,3) = translation;
		result.template block<3,3>(0,0) = rotation.matrix();
		return result;
	}

	/** @brief Gives identity */
	static Pose3 Identity() {
		return Pose3{{0,0,0}, So3<T>::Identity()};
	}

	/** @brief Gives pure translation transformation */
	static Pose3 Translation(const Vector3<T>& translation) {
		return Pose3{translation, So3<T>::Identity()};
	}
};

template<typename T>
std::ostream& operator<<(std::ostream& os, const Pose3<T>& pose) {
	const auto& t = pose.translation;
	const auto& r = pose.rotation;
	os << "Pose3{t=(" << t[0] << "," << t[1] << "," << t[2] << "), "
	   << "r=" << r << "}";
	return os;
}

/** @brief Multiplies two poses */
template<typename T>
Pose3<T> operator*(const Pose3<T>& a, const Pose3<T>& b) {
	return Pose3<T>{
		a.translation + a.rotation * b.translation,
		a.rotation * b.rotation
	};
}

/** @brief Computes "look at" transformation */
template<typename T>
Pose3<T> lookAt(const Vector3<T>& eye, const Vector3<T>& target, const Vector3<T>& up) {
	const Vector3<T> f = (target - eye).normalized();
	const Vector3<T> s = f.cross(up).normalized();
	const Vector3<T> u = s.cross(f);
	Eigen::Matrix3f rot;
	rot.block<3, 1>(0, 0) = s;
	rot.block<3, 1>(0, 1) = u;
	rot.block<3, 1>(0, 2) = -f;
	return Pose3<T>{eye, Eigen::Quaternion<T>(rot)}.inverse();
}

using Pose3f = Pose3<float>;
using Pose3d = Pose3<double>;

}
