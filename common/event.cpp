#include <fstream>
#include <sstream>

#include <snappy.h>

#include <dog/assert.hpp>

#include "event.hpp"

namespace ebvr {

/** @brief Gets the extension for a path to a file */
std::string FindFileExtension(const std::string& filename) {
	return filename.substr(filename.find_last_of(".") + 1);
}

void WriteToFile(const std::string& filename, const std::vector<Timed<Event>>& events) {
	std::string ext = FindFileExtension(filename);
	if(ext == "csv") {
		std::ofstream ofs(filename);
		dog::enforce(ofs, "Error writing file '" + filename + "'");
		WriteToStream(ofs, events);
	}
	else if(ext == "snappy") {
		// Write events to a string.
		std::stringstream ss;
		WriteToStream(ss, events);
		const std::string& str = ss.str();
		// Compress using snappy.
		std::string output;
		size_t len = snappy::Compress(str.c_str(), str.length(), &output);
		dog::enforce(len == str.length(), "Failed to compress events!");
		// Write compressed data to file.
		std::ofstream ofs(filename, std::ios::binary);
		dog::enforce(ofs, "Error writing file '" + filename + "'");
		ofs << output;
	} else {
		dog::enforce(false, "Unknown event file extension! Must be: .csv or .snappy");
	}
}

void ReadFromFile(const std::string& filename, std::vector<Timed<Event>>& events) {
	std::string ext = FindFileExtension(filename);
	if(ext == "csv") {
		std::ifstream ifs(filename);
		dog::enforce(ifs, "Error reading file '" + filename + "'");
		ReadFromStream(ifs, events);
	}
	else if(ext == "snappy") {
		// Read data from file.
		std::ifstream ifs(filename, std::ios::binary);
		dog::enforce(ifs, "Error reading file '" + filename + "'");
		std::string contents;
	    ifs.seekg(0, std::ios::end);
	    contents.resize(ifs.tellg());
	    ifs.seekg(0, std::ios::beg);
	    ifs.read(&contents[0], contents.size());
	    // Uncompress data using snappy.
		std::string input;
		bool ok = snappy::Uncompress(contents.c_str(), contents.length(), &input);
		dog::enforce(ok, "Failed to uncompress file contents!");
		// Parse events from uncompressed string.
		std::stringstream ss;
		ss.str(input);
		ReadFromStream(ss, events);
	} else {
		dog::enforce(false, "Unknown event file extension! Must be: .csv or .snappy");
	}
}

} // ebvr
